# About
___

MediFind Application is a simple data driven application that helps users locate the nearest HealthCare Facility. 
The aim of developing this application is so that users can log into the application, and have access to the following features 

+ Appointment History for user.
+ Ability to match users to facilities that accept the user's insurance details.
+ Find Facilities with a certain geographical location.

# Contributors
____
1. Rucha Damle 
2. Amala Joshy 
3. David Agumya

# Note
____

Northeastern University Seattle | Database Management System Course Team Project | Fall 2016

# Project Information 
___

** Check the Project Wiki for more information about the project ***